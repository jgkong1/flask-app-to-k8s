# Deploying a Python Flask Application to Kubernetes


# Introduction

This QuickLab will show you how to deploy a Python Flask application on to a Kubernetes cluster. We will build just the customary ‘hello-world’ application, as the focus of this QuickLab is deployment not Python coding. The technologies we would be working with are Flask, Docker, Kubernetes (IBM Cloud Kubernetes Service) and Eclipse Theia.

Flask is lightweight, minimalistic Python web framework designed to get an application up and running quickly. It is sometimes called a micro-framework as it does not require specific tools or plug-ins to run.

Docker is an open source application that allows us to package an application along with all it’s dependencies. Allowing us to create, manage, deploy, and replicate applications using containers. Docker defines a container as [follows:](https://www.docker.com/resources/what-container)

“*A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.”*

Any application deployed using Docker is isolated from the operating system resources and ensures that it works uniformly across various environments. Deploying our Flask application using Docker will allow us to replicate the application across different servers with minimal reconfiguration.

Kubernetes is another open source project that orchestrates the deployment of containerized applications. This orchestration allows for efficiency, scaling and abstraction of infrastructure. The [IBM Cloud Kubernetes Service (IKS)](https://cocl.us/ql_flask_k8s) is “a managed container service for the rapid delivery of applications that can bind to advanced services like IBM Watson® and blockchain.”

Eclipse Theia is an open source cloud IDE. It was inspired by VS Code and it supports many of it’s extensions. We would be using a pre-configured Theia [installation](https://labs.cognitiveclass.ai/tools/theiadocker/), optimized for deploying to IKS.


# Prerequisites

To follow this tutorial, you will need the following:

- An IBM Cloud account  – ([sign up for a free account](https://cloud.ibm.com/registration?cm_sp=ibmdev-_-developer-tutorials-_-cloudreg/))
- A basic knowledge of Flask and Python


# Building and Containerizing the Flask application

On Theia, create an `app` directory and enter it. Inside this directory create a `main.py`{: codeblock} file, and enter the following:


```python
from flask import Flask
    app = Flask(__name__)

    @app.route("/")
    def hello():
        return "Hello World from Flask in Theia"

    if __name__ == "__main__":
        # Only for debugging while developing
        app.run(host='0.0.0.0', debug=True, port=80)
```

{: codeblock}

Then create a `Dockerfile`{: codeblock} in your project directory. Enter the following:

```docker
FROM tiangolo/uwsgi-nginx-flask:python3.7
COPY ./app /app
```

{: codeblock}

On the first line we are importing the Docker image `tiangolo/uwsgi-nginx-flask` which comes with support for Python 2 and Python 3. This image allows us to create Flask web applications in Python that run with uWSGI and Nginx in a single container. We are interested in the latest version of this image available, which supports Python 3.

On the next line, we are copying the contents of the `app` directory we just created, into an `app` directory in the image.

You should have a directory structure like this:


![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581533230912_Screen+Shot+2020-02-12+at+1.44.33+PM.png)


While making sure you are in the `project` directory, where we have the `Dockerfile` and the `app` directory, run `docker build -t hello-world .`{: codeblock} in the Theia terminal. This builds the image, giving it a tag called `hello-world`. Next run the image using `docker run -d --name mycontainer -p 80:80 hello-world`{: codeblock}


![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581534765140_Screen+Shot+2020-02-12+at+2.11.27+PM.png)


Finally using `curl` we are able to see the output of our Flask application: `curl http://localhost`{: codeblock}


![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581535253916_Screen+Shot+2020-02-12+at+2.17.12+PM.png)



# Push the image to IBM Cloud registry

In this step we would push our image into the IBM Container Registry. We already have the IBM cloud CLI, Docker CLI and Container Registry plug-in pre-installed in Theia, so we would get started by first setting up our namespace.


- Log in to your IBM Cloud account.
    `ibmcloud login -a https://cloud.ibm.com`{: codeblock}
    If you have a federated ID, use `ibmcloud login --sso`{: codeblock} to log in to the IBM Cloud CLI.

- In the *Select a region* prompt, select `us-south`{: codeblock}

- You can ensure that you're targeting the correct IBM Cloud Container Registry region by running:
    `ibmcloud cr region-set us-south`{: codeblock}

- Create a namespace by running the following command. This namespace is your Container Registry namespace
    `ibmcloud cr namespace-add $(kubectl config view --minify --output 'jsonpath={..namespace}')`{: codeblock}


- To view your namespace run the command:
    `kubectl config view --minify --output 'jsonpath={..namespace}'`{: codeblock}


- Log your local Docker daemon into the IBM Cloud Container Registry.
    `ibmcloud cr login`{: codeblock}


- Select your repository and tag by which you can identify your image.
    `docker tag hello-world us.icr.io/<my_namespace>/hello-world:latest`{: codeblock}


- Push the image
    `docker push us.icr.io/<my_namespace>/hello-world:latest`{: codeblock}

![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581645332187_Screen+Shot+2020-02-13+at+8.54.50+PM.png)




# Deployment

Deployments are used to manage pods, which include containerized instances of an app. The following command deploys the app in a single pod by referring to the image that you built in your private registry. For the purposes of this tutorial, the deployment is named **hello-world-deployment**, but you can give the deployment any name that you want.

`kubectl create deployment hello-world-deployment --image=us.icr.io/sn-labs-caleboki/hello-world:latest`{: codeblock}

This would output:

`deployment "hello-world-deployment" created`

Next, make the app accessible to the world by exposing the deployment as a NodePort service. The NodePort that you expose is the port on which the worker node listens for traffic.

`kubectl expose deployment/hello-world-deployment --type=NodePort --port=80 --name=hello-world-service --target-port=80`{: codeblock}

| expose                                 | Expose a resource as a Kubernetes service and make it publicly available to users.                                                                |
| ----------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| deployment/``*<hello-world-deployment>* | The resource type and the name of the resource to expose with this service.                                                                       |
| --name=``*<hello-world-service>*        | The name of the service.                                                                                                                          |
| --port=``*<80>*                         | The port on which the service serves.                                                                                                             |
| --type=NodePort                         | The service type to create.                                                                                                                       |
| --target-port=``*<80>*                  | The port to which the service directs traffic. In this instance, the target-port is the same as the port, but other apps you create might differ. |

Now that all the deployment work is done, you can test your app in a browser. To do that, we need to get the details to form the URL.

First, get information about the service to see which NodePort was assigned.

`kubectl describe service hello-world-service`{: codeblock}

This would output:

| Name:                    | hello-world-service        |
| ------------------------ | -------------------------- |
| Namespace:               | default                    |
| Labels:                  | run=hello-world-deployment |
| Selector:                | run=hello-world-deployment |
| Type:                    | NodePort                   |
| IP:                      | 172.xxx.xx.xxx             |
| Port:                    | <unset> 80/TCP             |
| TargetPort:              | 80/TCP                     |
| NodePort:                | <unset> 31409/TCP          |
| Endpoints:               | <none>                     |
| Session Affinity:        | None                       |
| External Traffic Policy: | Cluster                    |
| Events:                  | <none>                     |

The NodePorts are randomly assigned when they are generated with the `expose` command, but within 30000-32767. In this example, the NodePort is 31409.

The cluster available to you is `labs-user-sandbox-prod-tor01`. To get the public IP address for the worker node in the cluster, run:

`ibmcloud ks worker ls --cluster labs-user-sandbox-prod-tor01`{: codeblock}

Open a browser and check out the app with the following URL: `http://<IP_address>:<NodePort>`. With the example values, the URL is `http://169.xx.xxx.xxx:31409`. When you enter that URL in a browser, you can see the following text.

`Hello World from Flask in Theia`


# Creating a Kubernetes cluster on IKS

A Kubernetes cluster is a set of node machines for running containerized applications. A cluster typically contains a worker node and a master node. The master node is responsible for maintaining the desired state of the cluster, such as which applications are running and which container images they use. Worker nodes actually run the applications and workloads.


- Sign into your IBM cloud account
- Open the menu pane and click on [**Kubernetes**](https://cloud.ibm.com/kubernetes/clusters)
- Click on the [**Create Cluster**](https://cloud.ibm.com/kubernetes/catalog/cluster/create) button on the top right hand side of the page
- Select the **Free cluster** option
- Select **Kubernetes** for cluster type and version
- In the **Cluster name** text box, enter a name for your cluster
- Then click on the **Create cluster** button


![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581538817889_Screen+Shot+2020-02-12+at+3.20.06+PM.png)


It takes some time for the cluster to get ready (around 30 minutes). In the meantime you would see this page:


![](https://paper-attachments.dropbox.com/s_DCFD61193E03B50259399C225F10F4C80461F0BEAA8668BBA6E65C16E8135AB7_1581539073555_Screen+Shot+2020-02-12+at+3.24.04+PM.png)


While the cluster is provisioning, run the following commands on your Theia terminal


- Download and install a few CLI tools and the Kubernetes Service plug-in.
            `curl -sL https://ibm.biz/idt-installer | bash`{: codeblock}
- Log in to your IBM Cloud account. Using the --sso option
            `ibmcloud login --sso`{: codeblock}
- Copy the One Time Code from the URL
    - At the prompt `One Time Code`, paste the code you copied
    - If you are authenticated successfully you would see a prompt to select a region. Enter `6` corresponding to the `us-south` region
- Download the kubeconfig files for your cluster.
            `ibmcloud ks cluster config --cluster mycluster`{: codeblock}
- Set the KUBECONFIG environment variable. Copy the output from the previous command and paste it in your terminal. The command output looks similar to the following example:

    export KUBECONFIG=/home/theia/.bluemix/plugins/container-service/clusters/bp25s5sd0sb6t9fsubv0/kube-config-hou02-mycluster.yml
- Now verify that you can connect to your cluster .
            `kubectl version --short`{: codeblock}



